git merge: permite a mesclagem de linhas de desenvolvimento independentes criadas pelo git branch e as integram em uma ramifica ̧c ̃ao ́unica.

git branch: comando para verificar todas as branches presentes no reposit ́orio. Ao utilizar a flag -r no final do comando ́e poss ́ıvel ver todas as branches presentes no reposit ́orio remoto. Al ́em disso, se vocˆe quiser criar uma nova branch, basta utilizar este comando: git branch ”nome da branch”.

git push: comando que permite ”subir”modifica ̧c ̃oes no nosso reposit ́orio remoto, para isso basta utilizar o comando git push e, se j ́a estiver tudo devidamente configurado, os arquivos ser ̃ao salvos no reposit ́orio remoto correspondente ao seu reposit ́orio local.

git pull: ́e usado para buscar e atualizar o conte ́udo de reposit ́orios remotos e fazer a atualiza ̧c ̃ao imediata ao reposit ́orio local para que os conte ́udos sejam iguais.

git add: este comando adiciona os arquivos solicitados ao ambiente de stage, ́e uma forma de dizer para o git que vocˆe deseja que as modifica ̧c ̃oes daquele arquivo sejam gravadas na pr ́oxima remessa. Um exemplo de utiliza ̧c ̃ao ́e: git add . onde o ponto representa todos os arquivos na pasta.

git commit: Realiza a grava ̧c ̃ao das modifica ̧c ̃oes do ambiente de stage. Desta forma, criamos uma representa ̧c ̃ao do estado atual do nosso projeto. Normalmente utilziamos o comando git commit -m “descri ̧c ̃ao das modifica ̧c ̃oes”, onde o -m representa o comando para inserirmos uma mensagem.

Git: ́e o sistema de versionamento em si, ́e onde vocˆe cria “commits” e pode registrar o hist ́orico de modifica ̧c ̃oes do seu projeto, criar “branches” e, caso algo dˆe errado, dar “rollback” para uma vers ̃ao anterior.

Github: ́e apenas uma das muitas centrais de reposit ́orios remotos existentes.

git checkout: comando utilizado para trocar de branch passando o nome da branch destino no final do comando. Caso a flag -b seja colocada ap ́os o “checkout” ́e poss ́ıvel criar a branch em quest ̃ao e j ́a trocar para esta imediatamente.

git status: comando que permite verificar quais arquivos est ̃ao sendo rastreados pelo git e quais modifica ̧c ̃oes j ́a foram enviadas para o stage. E bem ́util para quando se tem d ́uvidas sobre o que est ́a sendo ́ enviado.

git stage: comando que apresenta um espa ̧co tempor ́ario onde vocˆe determina quais mudan ̧cas ser ̃ao adicionadas.
